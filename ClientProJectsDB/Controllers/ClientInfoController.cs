﻿using ClientProJectsDB.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ClientProJectsDB.Controllers
{
    [Route("api/ClientInfo")]
    [ApiController]
    public class ClientInfoController : ControllerBase
    {
        private readonly ClientInfoContext _context;

        public ClientInfoController(ClientInfoContext context)
        {
            _context = context;
        }

        // GET: api/ClientInfo
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ClientInfoItem>>> GetClientInfoItems()
        {
            return await _context.ClientInfoItems.ToListAsync();
        }

        // GET: api/ClientInfo/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ClientInfoItem>> GetClientInfoItem(long id)
        {
            var ClientInfoItem = await _context.ClientInfoItems.FindAsync(id);

            if (ClientInfoItem == null)
            {
                return NotFound();
            }

            return ClientInfoItem;
        }

        // POST: api/ClientInfo
        [HttpPost]
        public async Task<ActionResult<ClientInfoItem>> PostClientInfoItem(ClientInfoItem item)
        {
            _context.ClientInfoItems.Add(item);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetClientInfoItem), new { id = item.Id }, item);
        }

        // PUT: api/ClientInfo/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutClientInfoItem(long id, ClientInfoItem item)
        {
            if (id != item.Id)
            {
                return BadRequest();
            }

            _context.Entry(item).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }

        // DELETE: api/ClientInfo/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteClientInfoItem(long id)
        {
            var ClientInfoItem = await _context.ClientInfoItems.FindAsync(id);

            if (ClientInfoItem == null)
            {
                return NotFound();
            }

            _context.ClientInfoItems.Remove(ClientInfoItem);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}