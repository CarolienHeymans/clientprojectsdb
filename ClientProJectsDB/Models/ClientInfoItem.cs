﻿namespace ClientProJectsDB.Models
{
    public class ClientInfoItem
    {
        public long Id { get; set; }
        public string clientName { get; set; }
        public string projectName { get; set; }
        public bool IsComplete { get; set; }
        public bool IsvrAccesible { get; set; }
    }
}