﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;


namespace ClientProJectsDB.Models
{
    public class ClientInfoContext : DbContext
    {
        public ClientInfoContext(DbContextOptions<ClientInfoContext> options)
            : base(options)
        {
        }

        public DbSet<ClientInfoItem> ClientInfoItems { get; set; }

    }
}
