﻿const uri = "api/ClientInfo";
let ClientInfos = null;
function getCount(data) {
    const el = $("#counter");
    let name = "Client";
    if (data) {
        if (data > 1) {
            name = "Clients";
        }
        el.text(data + " " + name);
    } else {
        el.text("No " + name);
    }
}

$(document).ready(function () {
    getData();
});

function getData() {
    $.ajax({
        type: "GET",
        url: uri,
        cache: false,
        success: function (data) {
            const tBody = $("#ClientInfos");

            $(tBody).empty();

            getCount(data.length);

            $.each(data, function (key, item) {
                const tr = $("<tr></tr>")
                    .append(
                        $("<td></td>").append(
                            $("<input/>", {
                                type: "checkbox",
                                disabled: true,
                                checked: item.isComplete
                            })
                        )
                    )
                    .append(
                        $("<td></td>").append(
                            $("<input/>", {
                                type: "checkbox",
                                disabled: true,
                                checked: item.IsvrAccesible
                            })
                        )
                    )
                    .append($("<td></td>").text(item.clientName))
                    .append($("<td></td>").text(item.projectName))
                    .append(
                        $("<td></td>").append(
                            $("<button>Edit</button>").on("click", function () {
                                editItem(item.id);
                            })
                        )
                    )
                    .append(
                        $("<td></td>").append(
                            $("<button>Delete</button>").on("click", function () {
                                deleteItem(item.id);
                            })
                        )
                    );

                tr.appendTo(tBody);
            });

            ClientInfos = data;
        }
    });
}

function addItem() {
    const item = {
        clientName: $("#add-name").val(),
        projectName: $("#add-project").val(),
        IsvrAccesible: $("#add-IsvrAccesible").is(':checked'),
        isComplete: false
    };
    console.log(item)
    $.ajax({
        type: "POST",
        accepts: "application/json",
        url: uri,
        contentType: "application/json",
        data: JSON.stringify(item),
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Something went wrong!");
        },
        success: function (result) {
            getData();
            $("#add-name").val("");
            $("#add-project").val("");
        }
    });
}

function deleteItem(id) {
    $.ajax({
        url: uri + "/" + id,
        type: "DELETE",
        success: function (result) {
            getData();
        }
    });
}

function editItem(id) {
    $.each(ClientInfos, function (key, item) {
        if (item.id === id) {
            $("#edit-name").val(item.clientName);
            $("#edit-project").val(item.projectName);
            $("#edit-id").val(item.id);
            $("#edit-isComplete")[0].checked = item.isComplete;
           // $("#edit- IsvrAccesible").is(':checked') = item.IsvrAccesible; -> needs fixing!!!
        }
    });
    $("#spoiler").css({ display: "block" });
}

$(".my-form").on("submit", function () {
    const item = {
        clientName: $("#edit-name").val(),
        projectName: $("#edit-project").val(),
        isComplete: $("#edit-isComplete").is(":checked"),
        IsvrAccesible: $("#edit-IsvrAccesible").is(":checked"),
        id: $("#edit-id").val()
    };

    $.ajax({
        url: uri + "/" + $("#edit-id").val(),
        type: "PUT",
        accepts: "application/json",
        contentType: "application/json",
        data: JSON.stringify(item),
        success: function (result) {
            getData();
        }
    });

    closeInput();
    return false;
});

function closeInput() {
    $("#spoiler").css({ display: "none" });
}